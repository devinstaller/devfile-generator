import { isEmpty, cloneDeep, merge } from 'lodash'
import { initialValues, DataSchema } from './data'

export function mergeWithInitalValues(data: any) {
  if (!isEmpty(data)) {
    const o = cloneDeep(initialValues)
    const p: DataSchema = merge(o, data)
    return p
  }
  return initialValues
}
