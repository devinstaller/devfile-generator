import { initialValues } from '@/core/data'
export default () => ({
  form: initialValues,
})
