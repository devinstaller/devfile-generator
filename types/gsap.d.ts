import Vue from 'vue'
import gsap from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'

declare module 'vue/types/vue' {
  interface Vue {
    $gsap: typeof gsap
    $ScrollTrigger: typeof ScrollTrigger
  }
}
