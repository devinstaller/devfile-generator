module.exports = {
  purge: ['./components/**/*.vue', './pages/**/*.vue'],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      colors: require('tailwindcss/colors'),
      fontFamily: {
        jost: "'Jost'",
        // playfair: "'Playfair Display'",
        poppins: "'Poppins'",
      },
      zIndex: {
        '-10': -10,
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require('@tailwindcss/forms')],
}
