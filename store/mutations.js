import Vue from 'vue'
export default {
  setForm(state, payload) {
    Vue.set(state, 'form', payload)
  },
}
