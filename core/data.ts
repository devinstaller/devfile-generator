import { ModuleTypes } from './models'

interface ModuleInstallInstruction {
  cmd: string
  rollback?: string
}
interface ConstantData {
  key: string
  value: string
}

export interface Module {
  rollback: boolean
  content: string
  alias: string
  create: boolean
  description: string
  display: string
  executable: string
  module_type: ModuleTypes
  name: string
  owner: string
  parent_dir: string
  permission: string
  source: string
  target: string
  url: string
  version: string
  before?: string
  after?: string
  symbolic: boolean
  constants?: ConstantData[]
  binds?: string[]
  supported_platforms: string[]
  optionals: string[]
  requires: string[]
  commands: string[] | ModuleInstallInstruction
  install_inst: ModuleInstallInstruction[]
  config: string[] | ModuleInstallInstruction
  init: string[] | ModuleInstallInstruction
}
export interface Include {
  spec_file: string
  prog_file?: string
}

export interface Constant {
  name: string
  inherits?: string[]
  data: ConstantData[]
}

export interface Platform {
  name: string
  description?: string
  platform_info: {
    system: string
    version: string
  }
}

export interface Interface {
  name: string
  before?: string
  after?: string
}
export interface DataSchema {
  version: string
  author: string
  description: string
  url: string
  prog_file: string
  includes: Include[]
  constants: Constant[]
  interfaces: Interface[]
  platforms: Platform[]
  modules: Module[]
}

export type SchemaBlocks =
  | DataSchema
  | Include
  | Constant
  | Interface
  | Platform
  | Module

export const initialValues: DataSchema = {
  version: '',
  author: '',
  description: '',
  url: '',
  prog_file: '',
  includes: [],
  constants: [],
  interfaces: [],
  platforms: [],
  modules: [],
}
