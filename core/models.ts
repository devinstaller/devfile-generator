import { get } from 'lodash'

export interface PushData {
  [key: string]: any
}

export type Name = (index: number[]) => string

type RenderFieldBlockType = 'block'
type RenderFieldSelectType = 'select'
type RenderFieldBooleanType = 'checkbox'
export type ModuleTypes = 'phony' | 'link' | 'group' | 'folder' | 'file' | 'app'

export interface RenderFieldBlock {
  name: string | Function
  label: string
  type: RenderFieldBlockType
  pushData: PushData | string
  only?: ModuleTypes[]
  data: RenderField[]
}

export interface RenderFieldInput {
  name: string | Function
  label: string
  placeholder?: string
  only?: ModuleTypes[]
  required?: boolean
  type?: 'text' | 'textarea'
  tooltip?: string
}

export interface RenderFieldBoolean {
  name: string | Function
  label: string
  type: RenderFieldBooleanType
  only?: ModuleTypes[]
  required?: boolean
  tooltip?: string
}

export interface RenderFieldSelect {
  type: RenderFieldSelectType
  name: string | Function
  label: string
  options: string[]
  defaultValue: string
  only?: ModuleTypes[]
  required?: boolean
  tooltip?: string
}

export type RenderField =
  | RenderFieldBlock
  | RenderFieldInput
  | RenderFieldSelect
  | RenderFieldBoolean

export const fullSchema: RenderField[] = [
  {
    name: 'version',
    label: 'Version',
    placeholder: 'v1',
    tooltip: "You don't need to worry about this for now.",
  },
  {
    name: 'author',
    label: 'Author',
    placeholder: 'John Doe',
    tooltip: 'Let everyone know who wrote this',
  },
  {
    name: 'description',
    label: 'Description',
    placeholder: 'Lorem Ipsum',
    type: 'textarea',
    tooltip: 'Describe your thoughts on this file.',
  },
  {
    name: 'url',
    label: 'URL',
    placeholder: 'https://dgenerate.aziraz.com',
    tooltip:
      'Maybe you want to give out the URL for your blog where you put some documentation on this.',
  },
  {
    name: 'prog_file',
    label: 'Prog file',
    placeholder: 'devfile.py',
    tooltip:
      "You will know when you are supposed to use this. Until then don't worry.",
  },
  {
    type: 'block',
    name: 'includes',
    label: 'Includes',
    pushData: { spec_file: '', prog_file: '' },
    data: [
      {
        name: (indices: number[]) => `includes.${get(indices, 0)}.spec_file`,
        placeholder: 'devfile.toml',
        label: 'Spec file',
        required: true,
      },
      {
        name: (indices: number[]) => `includes.${get(indices, 0)}.prog_file`,
        placeholder: 'devfile.py',
        label: 'Prog file',
      },
    ],
  },
  {
    type: 'block',
    name: 'constants',
    label: 'Constants',
    pushData: { name: '', inherits: [], pairs: [] },
    data: [
      {
        name: (index: number[]) => `constants.${index[0]}.name`,
        placeholder: '',
        label: 'Name',
        required: true,
      },
      {
        type: 'block',
        name: (index: number[]) => `constants.${index[0]}.inherits`,
        label: 'Inherits',
        pushData: '',
        data: [
          {
            name: (index: number[]) =>
              `constants.${index[0]}.inherits.${index[1]}`,
            label: '',
          },
        ],
      },
      {
        type: 'block',
        name: (index: number[]) => `constants.${index[0]}.pairs`,
        label: 'Pairs',
        pushData: { key: '', value: '' },
        data: [
          {
            name: (index: number[]) =>
              `constants.${index[0]}.pairs.${index[1]}.key`,
            label: 'Key',
          },
          {
            name: (index: number[]) =>
              `constants.${index[0]}.pairs.${index[1]}.value`,
            label: 'Value',
          },
        ],
      },
    ],
  },
  {
    type: 'block',
    name: 'interfaces',
    label: 'Interfaces',
    pushData: {
      name: '',
      description: '',
      before: '',
      after: '',
      before_each: '',
      after_each: '',
      modules: [],
    },
    data: [
      {
        name: (indices: number[]) => `interfaces.${get(indices, 0)}.name`,
        placeholder: '',
        label: 'Name',
        required: true,
      },
      {
        name: (indices: number[]) =>
          `interfaces.${get(indices, 0)}.description`,
        placeholder: 'Lorem Ipsum',
        label: 'Description',
        type: 'textarea',
      },
      {
        name: (indices: number[]) => `interfaces.${get(indices, 0)}.before`,
        placeholder: '',
        label: 'Before',
      },
      {
        name: (indices: number[]) => `interfaces.${get(indices, 0)}.after`,
        placeholder: '',
        label: 'After',
      },
      {
        name: (indices: number[]) =>
          `interfaces.${get(indices, 0)}.before_each`,
        placeholder: '',
        label: 'Before Each',
      },
      {
        name: (indices: number[]) => `interfaces.${get(indices, 0)}.after_each`,
        placeholder: '',
        label: 'After Each',
      },
      {
        type: 'block',
        name: (indices: number[]) => `interfaces.${get(indices, 0)}.modules`,
        label: 'Modules',
        pushData: { name: '', before: '', after: '' },
        data: [
          {
            name: (indices: number[]) =>
              `interfaces.${get(indices, 0)}.modules.${get(indices, 1)}.name`,
            placeholder: '',
            label: 'Name',
          },
          {
            name: (indices: number[]) =>
              `interfaces.${get(indices, 0)}.modules.${get(indices, 1)}.before`,
            placeholder: '',
            label: 'Before',
          },
          {
            name: (indices: number[]) =>
              `interfaces.${get(indices, 0)}.modules.${get(indices, 1)}.after`,
            placeholder: '',
            label: 'After',
          },
        ],
      },
    ],
  },
  {
    type: 'block',
    name: 'platforms',
    label: 'Platforms',
    pushData: {
      name: '',
      description: '',
      platform_infos: [],
    },
    data: [
      {
        name: (index: number[]) => `platforms.${index[0]}.name`,
        label: 'Name',
        required: true,
      },
      {
        name: (index: number[]) => `platforms.${index[0]}.description`,
        label: 'Description',
        type: 'textarea',
      },
      {
        type: 'block',
        name: (index: number[]) => `platforms.${index[0]}.platform_infos`,
        label: "Platform info's",
        pushData: { system: '', version: '' },
        data: [
          {
            name: (index: number[]) =>
              `platforms.${index[0]}.platform_infos.${index[1]}.system`,
            label: 'System',
            required: true,
          },
          {
            name: (index: number[]) =>
              `platforms.${index[0]}.platform_infos.${index[1]}.version`,
            label: 'Version',
          },
        ],
      },
    ],
  },
  {
    type: 'block',
    name: 'modules',
    label: 'Modules',
    pushData: {
      module_type: 'phony',
      supportedPlatforms: [],
      binds: [],
      constants: [],
      alias: '',
      create: false,
      rollback: false,
      inits: [],
      configs: [],
      installInsts: [],
      commands: [],
      uninstallInsts: [],
      contents: [],
      description: '',
      display: '',
      excutable: '',
      name: '',
      owner: '',
      parentDir: '',
      permission: '',
      requires: [],
      optionals: [],
      url: '',
      version: '',
      source: '',
      target: '',
      symbolic: false,
    },
    data: [
      {
        type: 'select',
        name: (index: number[]) => `modules.${index[0]}.module_type`,
        label: 'Module type',
        options: ['app', 'file', 'folder', 'link', 'group', 'phony'],
        defaultValue: 'phony',
        required: true,
      },
      {
        name: (index: number[]) => `modules.${index[0]}.name`,
        label: 'Name',
        required: true,
      },
      {
        name: (index: number[]) => `modules.${index[0]}.display`,
        label: 'Display',
      },
      {
        name: (index: number[]) => `modules.${index[0]}.alias`,
        label: 'Alias',
      },
      {
        name: (index: number[]) => `modules.${index[0]}.description`,
        label: 'Description',
        type: 'textarea',
      },
      {
        name: (index: number[]) => `modules.${index[0]}.url`,
        label: 'URL',
      },
      {
        name: (index: number[]) => `modules.${index[0]}.executable`,
        label: 'Executable',
        only: ['app'],
      },
      {
        name: (index: number[]) => `modules.${index[0]}.owner`,
        label: 'Owner',
        only: ['folder', 'file'],
      },
      {
        name: (index: number[]) => `modules.${index[0]}.parentDir`,
        label: 'Parent directory',
        only: ['folder', 'file'],
      },
      {
        name: (index: number[]) => `modules.${index[0]}.permission`,
        label: 'Permission',
        only: ['folder', 'file'],
      },
      {
        name: (index: number[]) => `modules.${index[0]}.version`,
        label: 'Version',
        only: ['app'],
      },
      {
        name: (index: number[]) => `modules.${index[0]}.source`,
        label: 'Source',
        only: ['link'],
      },
      {
        name: (index: number[]) => `modules.${index[0]}.target`,
        label: 'Target',
        only: ['link'],
      },
      {
        name: (index: number[]) => `modules.${index[0]}.symbolic`,
        label: 'Symbolic',
        type: 'checkbox',
        only: ['link'],
      },
      {
        type: 'checkbox',
        name: (index: number[]) => `modules.${index[0]}.create`,
        label: 'Create',
        only: ['link', 'folder', 'file'],
      },
      {
        type: 'checkbox',
        name: (index: number[]) => `modules.${index[0]}.rollback`,
        label: 'Rollback',
        only: ['link', 'folder', 'file'],
      },
      {
        type: 'block',
        name: (index: number[]) => `modules.${index[0]}.requires`,
        label: 'Requires',
        pushData: '',
        only: ['link', 'group', 'folder', 'file', 'app'],
        data: [
          {
            name: (index: number[]) =>
              `modules.${index[0]}.requires.${index[1]}`,
            label: '',
          },
        ],
      },
      {
        type: 'block',
        name: (index: number[]) => `modules.${index[0]}.optionals`,
        label: 'Optionals',
        pushData: '',
        only: ['link', 'group', 'folder', 'file', 'app'],
        data: [
          {
            name: (index: number[]) =>
              `modules.${index[0]}.optionals.${index[1]}`,
            label: '',
          },
        ],
      },
      {
        type: 'block',
        label: 'Supported platforms',
        name: (index: number[]) => `modules.${index[0]}.supportedPlatforms`,
        pushData: '',
        data: [
          {
            name: (index: number[]) =>
              `modules.${index[0]}.supportedPlatforms.${index[1]}`,
            label: '',
          },
        ],
      },
      {
        type: 'block',
        label: 'Binds',
        name: (index: number[]) => `modules.${index[0]}.binds`,
        pushData: '',
        data: [
          {
            name: (index: number[]) => `modules.${index[0]}.binds.${index[1]}`,
            label: '',
          },
        ],
      },
      {
        type: 'block',
        label: 'Constants',
        name: (index: number[]) => `modules.${index[0]}.constants`,
        pushData: { key: '', value: '' },
        data: [
          {
            name: (index: number[]) =>
              `modules.${index[0]}.constants.${index[1]}.key`,
            label: 'Key',
            required: true,
          },
          {
            name: (index: number[]) =>
              `modules.${index[0]}.constants.${index[1]}.value`,
            label: 'Value',
            required: true,
          },
        ],
      },
      {
        type: 'block',
        name: (index: number[]) => `modules.${index[0]}.inits`,
        label: 'Init',
        pushData: { cmd: '', rollback: '' },
        only: ['link', 'folder', 'file'],
        data: [
          {
            label: 'CMD',
            name: (index: number[]) =>
              `modules.${index[0]}.inits.${index[1]}.cmd`,
            required: true,
          },
          {
            label: 'Rollback',
            name: (index: number[]) =>
              `modules.${index[0]}.inits.${index[1]}.rollback`,
          },
        ],
      },
      {
        type: 'block',
        name: (index: number[]) => `modules.${index[0]}.configs`,
        label: 'Configs',
        pushData: { cmd: '', rollback: '' },
        only: ['link', 'folder', 'file'],
        data: [
          {
            label: 'CMD',
            name: (index: number[]) =>
              `modules.${index[0]}.configs.${index[1]}.cmd`,
            required: true,
          },
          {
            label: 'Rollback',
            name: (index: number[]) =>
              `modules.${index[0]}.configs.${index[1]}.rollback`,
          },
        ],
      },
      {
        type: 'block',
        name: (index: number[]) => `modules.${index[0]}.commands`,
        label: 'Commands',
        pushData: '',
        only: ['phony'],
        data: [
          {
            label: '',
            name: (index: number[]) =>
              `modules.${index[0]}.commands.${index[1]}`,
          },
        ],
      },
      {
        type: 'block',
        name: (index: number[]) => `modules.${index[0]}.installInsts`,
        label: 'Install Instructions',
        pushData: { cmd: '', rollback: '' },
        only: ['app'],
        data: [
          {
            label: 'CMD',
            name: (index: number[]) =>
              `modules.${index[0]}.installInsts.${index[1]}.cmd`,
            required: true,
          },
          {
            label: 'Rollback',
            name: (index: number[]) =>
              `modules.${index[0]}.installInsts.${index[1]}.rollback`,
          },
        ],
      },
      {
        type: 'block',
        name: (index: number[]) => `modules.${index[0]}.uninstallInsts`,
        label: 'Uninstall Instructions',
        pushData: '',
        only: ['app'],
        data: [
          {
            label: '',
            name: (index: number[]) =>
              `modules.${index[0]}.uninstallInsts.${index[1]}`,
          },
        ],
      },
      {
        type: 'block',
        name: (index: number[]) => `modules.${index[0]}.contents`,
        label: 'Contents',
        pushData: { digest: '', path: '' },
        only: ['file'],
        data: [
          {
            label: 'Digest',
            name: (index: number[]) =>
              `modules.${index[0]}.contents.${index[1]}.digest`,
          },
          {
            label: 'Path',
            name: (index: number[]) =>
              `modules.${index[0]}.contents.${index[1]}.path`,
            required: true,
          },
        ],
      },
    ],
  },
]
